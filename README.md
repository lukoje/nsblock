# *NSBlock* 
Simple cross-platform python script for generating host files to block ads, trackers and other nasty things.
  
## Installation
```
python setup.py install
```
## How to use *nsblock*
```
usage: nsblock [-h] [-f file [file ...] | -r] [-o path] [-t addr]

optional arguments:
  -h, --help          show this help message and exit
  -f file [file ...]  take rules from local (host) files.
  -r                  revert all changes.
  -o path             path to the host file. (default: C:\Windows\System32\drivers\etc\hosts or /etc/hosts)
  -t addr             the address to which dns will be resolved (defalut: 127.0.0.1).
```
```
Examples: nsblock
          nsblock -f file1 file2 file3 -t 0.0.0.0
```
